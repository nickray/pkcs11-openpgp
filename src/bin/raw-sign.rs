use cryptoki::types::locking::CInitializeArgs;
use cryptoki::types::session::UserType;
use cryptoki::types::slot_token::Slot;
use cryptoki::types::Flags;
use cryptoki::Pkcs11;

use cryptoki::types::mechanism::Mechanism;
use cryptoki::types::object::{Attribute, AttributeType};
use std::env;
use std::convert::TryInto;

pub fn init_pins() -> Result<(Pkcs11, Slot), Box<dyn std::error::Error>> {
    let pkcs11 = Pkcs11::new(
        env::var("PKCS11_SOFTHSM2_MODULE")
            .unwrap_or_else(|_|
			    //"/usr/lib/softhsm/libsofthsm2.so"
			    "/usr/lib/libykcs11.so"
			    .to_string()),
    )?;

    // initialize the library
    pkcs11.initialize(CInitializeArgs::OsThreads)?;

    println!("Get slots: {:?}", pkcs11.get_slots_with_token()?);

    // find a slot, get the first one
    let slot = 0x0.try_into()?;//pkcs11.get_slots_with_token().unwrap();//.remove(0);
    println!("Slot: {:?}", slot);

//    let slot = slots.remove(0);

    //pkcs11.init_token(slot, "112233").unwrap();
    pkcs11.set_pin(slot, "112233")?;

    // set flags
    let mut flags = Flags::new();
    let _ = flags.set_rw_session(false).set_serial_session(true);

    {
        // open a session
        let session = pkcs11.open_session_no_callback(slot, flags)?;
        // log in the session
        session.login(UserType::User)?;
        //session.init_pin("112233").unwrap();
	//session.set_pin("112233").unwrap();
    }

    println!("Slot OK.");

    Ok((pkcs11, slot))
}

fn sign_verify() -> Result<(), Box<dyn std::error::Error>> {
    let (pkcs11, slot) = init_pins()?;

    // set flags
    let mut flags = Flags::new();
    let _ = flags.set_rw_session(true).set_serial_session(true);

    // open a session
    let session = pkcs11.open_session_no_callback(slot, flags)?;

    // log in the session
    session.login(UserType::User)?;

    // get mechanism
    let _mechanism = Mechanism::RsaPkcsKeyPairGen;

    let public_exponent: Vec<u8> = vec![0x01, 0x00, 0x01];
    let modulus_bits = 2048;

    // pub key template
    let _pub_key_template = vec![
        Attribute::Token(true.into()),
        Attribute::Private(true.into()),
        Attribute::PublicExponent(public_exponent),
        Attribute::ModulusBits(modulus_bits.into()),
    ];

    // priv key template
    let priv_key_template = vec![Attribute::Token(true.into()),
				 Attribute::Private(true.into()), Attribute::Sign(true.into())];

    // generate a key pair
    //let (public, private) = session
    //    .generate_key_pair(&mechanism, &pub_key_template, &priv_key_template)?;

    let objects = session.find_objects(&priv_key_template)?;

    println!("{:?}", objects);

    for object in &objects {
	println!("Object: {:?}", object);
	let attributes = session.get_attributes(*object, &[AttributeType::Label]);
	println!("Attributes: {:?}", attributes);
	if let Ok(values) = attributes {
	if let Attribute::Label(str) = &values[0] {
	    println!(" STR = {}", String::from_utf8_lossy(&str));
	}
	}
	println!();
    }
    // data to sign
    let data = "foo\n".as_bytes();

    // sign something with it
    let signature = session.sign(&Mechanism::RsaPkcs, objects[0], &data)?;
    println!("{:X?}", signature);
    println!("len = {}", signature.len());
    let mut file = std::fs::OpenOptions::new().write(true).create(true).open("sig.bin")?;
    use std::io::Write;
    file.write_all(&signature)?;
/*
    // verify the signature
    session
        .verify(&Mechanism::RsaPkcs, public, &data, &signature)?;

    // delete keys
    session.destroy_object(public)?;
    session.destroy_object(private)?;
*/
    Ok(())
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    println!("hello folks!");
    sign_verify()
}
