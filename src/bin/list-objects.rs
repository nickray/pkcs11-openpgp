use cryptoki::types::locking::CInitializeArgs;
use cryptoki::types::session::UserType;
use cryptoki::types::slot_token::Slot;
use cryptoki::types::object::KeyType;
use cryptoki::types::Flags;
use cryptoki::Pkcs11;

use cryptoki::types::object::{Attribute, AttributeType};
use std::env;

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "list-objects")]
struct Opt {
    /// PIN to access the card
    #[structopt(short, long, default_value = "112233")]
    pin: String,
}

pub fn init_pins(pin: String) -> Result<(Pkcs11, Slot), Box<dyn std::error::Error>> {
    let pkcs11 = Pkcs11::new(
        env::var("PKCS11_SOFTHSM2_MODULE")
            .unwrap_or_else(|_|
			    //"/usr/lib/softhsm/libsofthsm2.so"
			    "/usr/lib/libykcs11.so"
			    .to_string()),
    )?;

    // initialize the library
    pkcs11.initialize(CInitializeArgs::OsThreads)?;

    eprintln!("Get slots: {:?}", pkcs11.get_slots_with_token()?);

    // find a slot, get the first one
    let slot = pkcs11.get_slots_with_token().unwrap().remove(0);
    eprintln!("Slot: {:?}", slot);

    pkcs11.set_pin(slot, &pin)?;

    // set flags
    let mut flags = Flags::new();
    let _ = flags.set_rw_session(false).set_serial_session(true);

    {
        // open a session
        let session = pkcs11.open_session_no_callback(slot, flags)?;
        // log in the session
        session.login(UserType::User)?;
    }

    eprintln!("Slot OK.");

    Ok((pkcs11, slot))
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();

    let (pkcs11, slot) = init_pins(opt.pin)?;

    // set flags
    let mut flags = Flags::new();
    let _ = flags.set_rw_session(true).set_serial_session(true);

    // open a session
    let session = pkcs11.open_session_no_callback(slot, flags)?;

    // log in the session
    session.login(UserType::User)?;

    let public_exponent: Vec<u8> = vec![0x01, 0x00, 0x01];
    let modulus_bits = 2048;

    // pub key template
    let pub_key_template = vec![
        Attribute::Token(true.into()),
        //Attribute::Private(false.into()),
        Attribute::PublicExponent(public_exponent),
        Attribute::ModulusBits(modulus_bits.into()),
	Attribute::KeyType(KeyType::RSA.into()),
	//Attribute::Class(ObjectClass::PUBLIC_KEY.into()),
    ];

    let objects = session.find_objects(&pub_key_template)?;

    for object in &objects {
	eprintln!("Object: {:?}", object);
	let values = session.get_attributes(*object, &[AttributeType::Id, AttributeType::Label])?;
	for value in &values {
	    match value {
		Attribute::Label(str) => 
		    eprintln!(" Label: {}", String::from_utf8_lossy(&str)),
	        Attribute::Id(id) => 
		    eprintln!(" ID: {:?}", id),
		_ => {},
	    }
	}
	eprintln!();
    }
    Ok(())
}
