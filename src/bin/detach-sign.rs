use cryptoki::types::object::ObjectHandle;
use cryptoki::types::session::{Session, UserType};
use cryptoki::types::Flags;

use cryptoki::types::mechanism::Mechanism;
use cryptoki::types::object::{Attribute, AttributeType};
use std::sync::{Arc, Mutex};

use std::{
    io,
    time::SystemTime,
};

use sequoia_openpgp as openpgp;

use openpgp::{
    packet::{
        key::{PublicParts, UnspecifiedRole},
        prelude::*,
    },
    serialize::stream::{Armorer, Message, Signer},
    types::HashAlgorithm,
    Error,
};

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "detach-sign")]
struct Opt {
    /// PIN to access the card
    #[structopt(short, long, default_value = "112233")]
    pin: String,

    #[structopt(long, default_value = "0")]
    index: usize,

    #[structopt(short, long)]
    id: u8,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();
    let (pkcs11, slot) = pkcs11_openpgp::init_pins(&opt.pin)?;

    // set flags
    let mut flags = Flags::new();
    let _ = flags.set_rw_session(true).set_serial_session(true);

    // open a session
    let session = pkcs11.open_session_no_callback(slot, flags)?;

    // log in the session
    session.login(UserType::User)?;

    let public_exponent: Vec<u8> = vec![0x01, 0x00, 0x01];
    let modulus_bits = 2048;

    // pub key template
    let pub_key_template = vec![
        Attribute::Token(true.into()),
        Attribute::Private(false.into()),
        Attribute::PublicExponent(public_exponent),
        Attribute::ModulusBits(modulus_bits.into()),
	Attribute::Id(vec![opt.id].into()),
    ];

    let mut objects = session.find_objects(&pub_key_template)?;

    eprintln!("{:?}", objects);

    for object in &objects {
	eprintln!("Object: {:?}", object);
	let attributes = session.get_attributes(*object, &[AttributeType::Label]);
	eprintln!("Attributes: {:?}", attributes);
	if let Ok(values) = attributes {
	if let Attribute::Label(str) = &values[0] {
	    eprintln!(" STR = {}", String::from_utf8_lossy(&str));
	}
	}
	eprintln!();
    }
    // data to sign

    let object = objects.remove(opt.index);

    let attributes = session.get_attributes(object, &[AttributeType::Modulus, AttributeType::PublicExponent])?;

    let mut key_modulus: Option<Vec<u8>> = None;
    let mut key_exponent: Option<Vec<u8>> = None;

    for attribute in attributes {
	match attribute {
	    Attribute::Modulus(modulus) => key_modulus = Some(modulus),
	    Attribute::PublicExponent(exponent) => key_exponent = Some(exponent),
	    _ => {},
	}
    }

    if let (Some(modulus), Some(exponent)) = (key_modulus, key_exponent) {
        let key4: Key<PublicParts, UnspecifiedRole> =
            Key4::import_public_rsa(&exponent, &modulus, SystemTime::UNIX_EPOCH)?.into();


    // priv key template
    let priv_key_template = vec![Attribute::Token(true.into()),
				 Attribute::Private(true.into()), Attribute::Sign(true.into()), Attribute::Id(vec![opt.id].into())];

    let mut objects = session.find_objects(&priv_key_template)?;

    eprintln!("prv = {:?}", objects);

    for object in &objects {
	eprintln!("prv = Object: {:?}", object);
	let attributes = session.get_attributes(*object, &[AttributeType::Label]);
	eprintln!("prv = Attributes: {:?}", attributes);
	if let Ok(values) = attributes {
	if let Attribute::Label(str) = &values[0] {
	    eprintln!("prv = STR = {}", String::from_utf8_lossy(&str));
	}
	}
	eprintln!();
    }

        sign(&key4, session, &objects.remove(0))?;

        Ok(())
    } else {
        Err(Error::InvalidOperation(format!("Can handle only RSA key for now.")).into())
    }
}

/// Decrypts the given message.
fn sign<'a>(
    public: &'a Key<PublicParts, UnspecifiedRole>,
    session: Session<'a>,
    handle: &'a ObjectHandle,
) -> openpgp::Result<()> {
    let message = Message::new(io::stdout());

    let message = Armorer::new(message).build()?;

    // Now, create a signer that emits the signature(s).
    let mut signer = Signer::new(
        message,
        PkcsKeyPair {
            session: Arc::new(Mutex::new(session)),
            object: &handle,
            public,
        },
    );
    signer = signer.hash_algo(HashAlgorithm::SHA256)?;
    let mut signer = signer.detached().build()?;

    // Then, create a literal writer to wrap the data in a literal
    // message packet.
    //let mut literal = LiteralWriter::new(signer).build()?;

    // Copy all the data.
    io::copy(&mut io::stdin(), &mut signer)?;

    // Finally, teardown the stack to ensure all the data is written.
    signer.finalize()?;
    Ok(())
}


struct PkcsKeyPair<'a> {
    public: &'a Key<PublicParts, UnspecifiedRole>,
    object: &'a ObjectHandle,
    session: Arc<Mutex<Session<'a>>>,
}

impl openpgp::crypto::Signer for PkcsKeyPair<'_> {
    fn public(&self) -> &Key<PublicParts, UnspecifiedRole> {
        self.public
    }

    fn sign(
        &mut self,
        hash_algo: HashAlgorithm,
        digest: &[u8],
    ) -> openpgp::Result<openpgp::crypto::mpi::Signature> {
        assert_eq!(hash_algo, HashAlgorithm::SHA256);

	eprintln!("Key handle = {:?}", self.object);

	let session = Arc::clone(&self.session);
	let session = session.lock().unwrap();

	use picky_asn1_x509::{AlgorithmIdentifier, DigestInfo, SHAVariant};

	let digest: Vec<_> = digest.into();
	
	let info = picky_asn1_der::to_vec(&DigestInfo {
	    oid: AlgorithmIdentifier::new_sha(SHAVariant::SHA2_256),
	    digest: digest.into(),
	})?;
	
	let signature = session.sign(&Mechanism::RsaPkcs, *self.object, &info)?;

        Ok(openpgp::crypto::mpi::Signature::RSA { s: signature.into() })
    }
}

