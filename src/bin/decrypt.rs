use cryptoki::types::{locking::CInitializeArgs, object::ObjectHandle};
use cryptoki::types::session::{Session, UserType};
use cryptoki::types::slot_token::Slot;
use cryptoki::types::Flags;
use cryptoki::Pkcs11;

use cryptoki::types::mechanism::Mechanism;
use cryptoki::types::object::{Attribute, AttributeType};


use std::{
    convert::TryInto,
    io,
    time::SystemTime,
    env,
};

use sequoia_openpgp as openpgp;

use openpgp::{
    crypto::SessionKey,
    packet::{
        key::{UnspecifiedRole, PublicParts},
        prelude::*,
    },
    parse::{
        stream::{DecryptionHelper, DecryptorBuilder, MessageStructure, VerificationHelper},
        Parse,
    },
    policy::NullPolicy,
    types::SymmetricAlgorithm,
};


pub fn init_pins() -> Result<(Pkcs11, Slot), Box<dyn std::error::Error>> {
    let pkcs11 = Pkcs11::new(
        env::var("PKCS11_SOFTHSM2_MODULE")
            .unwrap_or_else(|_|
			    //"/usr/lib/softhsm/libsofthsm2.so"
			    "/usr/lib/libykcs11.so"
			    .to_string()),
    )?;

    // initialize the library
    pkcs11.initialize(CInitializeArgs::OsThreads)?;

    eprintln!("Get slots: {:?}", pkcs11.get_slots_with_token()?);

    // find a slot, get the first one
    let slot = 0x0.try_into()?;//pkcs11.get_slots_with_token().unwrap();//.remove(0);
    eprintln!("Slot: {:?}", slot);

//    let slot = slots.remove(0);

    //pkcs11.init_token(slot, "112233").unwrap();
    pkcs11.set_pin(slot, "112233")?;

    // set flags
    let mut flags = Flags::new();
    let _ = flags.set_rw_session(false).set_serial_session(true);

    {
        // open a session
        let session = pkcs11.open_session_no_callback(slot, flags)?;
        // log in the session
        session.login(UserType::User)?;
        //session.init_pin("112233").unwrap();
	//session.set_pin("112233").unwrap();
    }

    eprintln!("Slot OK.");

    Ok((pkcs11, slot))
}


fn main() -> Result<(), Box<dyn std::error::Error>> {

    let (pkcs11, slot) = init_pins()?;

    // set flags
    let mut flags = Flags::new();
    let _ = flags.set_rw_session(true).set_serial_session(true);

    // open a session
    let session = pkcs11.open_session_no_callback(slot, flags)?;

    // log in the session
    session.login(UserType::User)?;

    // get mechanism
    let _mechanism = Mechanism::RsaPkcsKeyPairGen;

    let public_exponent: Vec<u8> = vec![0x01, 0x00, 0x01];
    let modulus_bits = 2048;

    // pub key template
    let pub_key_template = vec![
        Attribute::Token(true.into()),
        Attribute::Private(false.into()),
        Attribute::PublicExponent(public_exponent),
        Attribute::ModulusBits(modulus_bits.into()),
    ];

    let mut objects = session.find_objects(&pub_key_template)?;

    eprintln!("{:?}", objects);

    for object in &objects {
	eprintln!("Object: {:?}", object);
	let attributes = session.get_attributes(*object, &[AttributeType::Label]);
	eprintln!("Attributes: {:?}", attributes);
	if let Ok(values) = attributes {
	if let Attribute::Label(str) = &values[0] {
	    eprintln!(" STR = {}", String::from_utf8_lossy(&str));
	}
	}
	eprintln!();
    }
    // data to sign

    let object = objects.remove(0);

    let attributes = session.get_attributes(object, &[AttributeType::Modulus, AttributeType::PublicExponent])?;

    let mut key_modulus: Option<Vec<u8>> = None;
    let mut key_exponent: Option<Vec<u8>> = None;

    for attribute in attributes {
	match attribute {
	    Attribute::Modulus(modulus) => key_modulus = Some(modulus),
	    Attribute::PublicExponent(exponent) => key_exponent = Some(exponent),
	    _ => {},
	}
    }

    if let (Some(modulus), Some(exponent)) = (key_modulus, key_exponent) {
        let _key4: Key<PublicParts, UnspecifiedRole> =
            Key4::import_public_rsa(&exponent, &modulus, SystemTime::UNIX_EPOCH)?.into();


    // priv key template
    let priv_key_template = vec![Attribute::Token(true.into()),
				 Attribute::Private(true.into()), Attribute::Sign(true.into())];

    // generate a key pair
    //let (public, private) = session
    //    .generate_key_pair(&mechanism, &pub_key_template, &priv_key_template)?;

    let mut objects = session.find_objects(&priv_key_template)?;

    eprintln!("prv = {:?}", objects);

    for object in &objects {
	eprintln!("prv = Object: {:?}", object);
	let attributes = session.get_attributes(*object, &[AttributeType::Label]);
	eprintln!("prv = Attributes: {:?}", attributes);
	if let Ok(values) = attributes {
	if let Attribute::Label(str) = &values[0] {
	    eprintln!("prv = STR = {}", String::from_utf8_lossy(&str));
	}
	}
	eprintln!();
    }

	let handle = objects.remove(1);

    let helper = Helper {
        session,
	handle: &handle,
    };

    // Now, create a decryptor with a helper using the given Certs.
	let stdin = std::io::stdin();
	let ref policy = NullPolicy::new();
    let mut decryptor = DecryptorBuilder::from_reader(stdin)?.with_policy(policy, None, helper)?;

    // Decrypt the data.
    let mut stdout = std::io::stdout();
    io::copy(&mut decryptor, &mut stdout)?;

	
        Ok(())
    } else {
        Err(openpgp::Error::InvalidOperation(format!("Can handle only RSA key for now.")).into())
    }
}

struct Helper<'a> {
    session: Session<'a>,
    handle: &'a ObjectHandle,
}

impl<'a> VerificationHelper for Helper<'a> {
    fn get_certs(&mut self, _ids: &[openpgp::KeyHandle]) -> openpgp::Result<Vec<openpgp::Cert>> {
        // Return public keys for signature verification here.
        Ok(Vec::new())
    }

    fn check(&mut self, _structure: MessageStructure) -> openpgp::Result<()> {
        // Implement your signature verification policy here.
        Ok(())
    }
}

impl<'a> DecryptionHelper for Helper<'a> {
    fn decrypt<D>(
        &mut self,
        pkesks: &[openpgp::packet::PKESK],
        _skesks: &[openpgp::packet::SKESK],
        sym_algo: Option<SymmetricAlgorithm>,
        mut decrypt: D,
    ) -> openpgp::Result<Option<openpgp::Fingerprint>>
    where
        D: FnMut(SymmetricAlgorithm, &SessionKey) -> bool,
    {
        let mut pair = PkcsKeyPair {
	    session: &self.session,
	    handle: self.handle,
        };
        //key.into_keypair().unwrap();

        pkesks[0]
            .decrypt(&mut pair, sym_algo)
            .map(|(algo, session_key)| decrypt(algo, &session_key));

        // XXX: In production code, return the Fingerprint of the
        // recipient's Cert here
        Ok(None)
    }
}

struct PkcsKeyPair<'a> {
    session: &'a Session<'a>,
    handle: &'a ObjectHandle,
}

impl openpgp::crypto::Decryptor for PkcsKeyPair<'_> {
    fn public(&self) -> &Key<PublicParts, key::UnspecifiedRole> {
        //self.key
        panic!("No biggie");
    }

    fn decrypt(
        &mut self,
        ciphertext: &openpgp::crypto::mpi::Ciphertext,
        _plaintext_len: Option<usize>,
    ) -> openpgp::Result<SessionKey> {
        use openpgp::crypto::mpi::Ciphertext::*;
        if let RSA { c } = ciphertext {
            let bytes = c.value();
	    let decrypted = self.session.decrypt(&Mechanism::RsaPkcs, *self.handle, &bytes)?;
            eprintln!("Decrypted session key = {:X?}", decrypted);
            Ok(decrypted.as_slice().into())
        } else {
            Err(openpgp::Error::InvalidOperation(format!(
                "Don't know how to handle non-RSA things."
            ))
            .into())
        }
    }
}
