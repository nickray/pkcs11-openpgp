use std::{io, path::PathBuf, time::SystemTime};

use openpgp::{
    packet::{signature, Signature},
    parse::Parse,
    serialize::Marshal,
    types::{Features, HashAlgorithm, KeyFlags, SignatureType},
    Cert, Error, Packet, PacketPile,
};
use sequoia_openpgp as openpgp;

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "bind-subkey")]
struct Opt {
    // Backsignature created by the signing key.
    #[structopt(short, long, parse(from_os_str))]
    backsig: Option<PathBuf>,

    // Public parts of the signing subkey.
    #[structopt(short, long, parse(from_os_str))]
    subkey: PathBuf,
}

fn main() -> openpgp::Result<()> {
    let opt = Opt::from_args();

    let stdin = io::stdin();
    let cert = Cert::from_reader(stdin)?;
    let c2 = cert.clone();
    let k1 = c2.keys().unencrypted_secret().nth(0).unwrap().key().clone();
    let mut signer = k1.into_keypair()?;

    // SIGNING SUBKEY
    let mut signing_builder = signature::SignatureBuilder::new(SignatureType::SubkeyBinding)
        .set_signature_creation_time(SystemTime::now())?
        // TPM requires SHA256 for now.
        .set_hash_algo(HashAlgorithm::SHA256)
        .set_features(Features::sequoia())?;
     
    let backsig = opt.backsig.map(|backsig| Signature::from_file(backsig).unwrap());

    if let Some(backsig) = backsig {
	signing_builder = signing_builder.set_embedded_signature(backsig)?
	    .set_key_flags(KeyFlags::empty().set_signing())?;
    } else {
	signing_builder = signing_builder.set_key_flags(KeyFlags::empty().set_transport_encryption().set_storage_encryption())?;
    }

    let pp = PacketPile::from_file(opt.subkey)?;
    if let Some(Packet::PublicSubkey(ref signing_key)) = pp.path_ref(&[0]) {
        let signing_bsig: Packet = signing_key
            .bind(&mut signer, &cert, signing_builder)?
            .into();

        let mut stdout = io::stdout();
        signing_bsig.serialize(&mut stdout)?;

        Ok(())
    } else {
        Err(Error::InvalidOperation(format!(
            "No public subkey packet. Did you use correct file?"
        ))
        .into())
    }
}
