use cryptoki::types::object::ObjectClass;
use cryptoki::types::session::UserType;
use cryptoki::types::object::KeyType;
use cryptoki::types::Flags;

use cryptoki::types::object::{Attribute, AttributeType};
use std::time::SystemTime;
use std::io;

use sequoia_openpgp as openpgp;
use openpgp::packet::{Packet, Key};
use openpgp::packet::key::{Key4, PublicParts, SubordinateRole};
use openpgp::serialize::Serialize;

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "export-subkey")]
struct Opt {
    /// PIN to access the card
    #[structopt(short, long, default_value = "112233")]
    pin: String,

    #[structopt(long, default_value = "0")]
    index: usize,

    #[structopt(short, long)]
    id: u8,
}


fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();

    let (pkcs11, slot) = pkcs11_openpgp::init_pins(&opt.pin)?;

    // set flags
    let mut flags = Flags::new();
    let _ = flags.set_rw_session(true).set_serial_session(true);

    // open a session
    let session = pkcs11.open_session_no_callback(slot, flags)?;

    // log in the session
    session.login(UserType::User)?;

    let public_exponent: Vec<u8> = vec![0x01, 0x00, 0x01];
    let modulus_bits = 2048;

    // pub key template
    let pub_key_template = vec![
        Attribute::Token(true.into()),
        Attribute::Private(false.into()),
        Attribute::PublicExponent(public_exponent),
        Attribute::ModulusBits(modulus_bits.into()),
	Attribute::KeyType(KeyType::RSA.into()),
	Attribute::Class(ObjectClass::PUBLIC_KEY.into()),
	Attribute::Id(vec![opt.id].into()),
    ];

    let mut objects = session.find_objects(&pub_key_template)?;

    eprintln!("{:?}", objects);

    for object in &objects {
	eprintln!("Object: {:?}", object);
	let attributes = session.get_attributes(*object, &[AttributeType::Label]);
	eprintln!("Attributes: {:?}", attributes);
	if let Ok(values) = attributes {
	if let Attribute::Label(str) = &values[0] {
	    eprintln!(" STR = {}", String::from_utf8_lossy(&str));
	}
	}
	eprintln!();
    }

    let object = objects.remove(opt.index);

    let attributes = session.get_attributes(object, &[AttributeType::Modulus, AttributeType::PublicExponent, AttributeType::Label])?;

    let mut key_modulus: Option<Vec<u8>> = None;
    let mut key_exponent: Option<Vec<u8>> = None;

    for attribute in attributes {
	match attribute {
	    Attribute::Modulus(modulus) => {
		eprintln!("GOT MODULUS = {:X?}", modulus);
		key_modulus = Some(modulus)
	    },
	    Attribute::PublicExponent(exponent) => {
		eprintln!("GOT EXPONENT = {:X?}", exponent);
		key_exponent = Some(exponent);
	    },
	    Attribute::Label(str) => eprintln!("USING = {}", String::from_utf8_lossy(&str)),
	    _ => {},
	}
    }

    if let (Some(modulus), Some(exponent)) = (key_modulus, key_exponent) {
        let key4: Key<PublicParts, SubordinateRole> =
            Key4::import_public_rsa(&exponent, &modulus, SystemTime::UNIX_EPOCH)?.into();

	eprintln!("Key4 = {:?}", key4);
	
        let packet: Packet = key4.into();
        let mut stdout = io::stdout();
        packet.serialize(&mut stdout)?;
    } else {
	eprintln!("did not get both");
    }
    
    Ok(())
}
