use cryptoki::types::object::ObjectHandle;
use cryptoki::types::session::{Session, UserType};
use cryptoki::types::Flags;

use cryptoki::types::mechanism::Mechanism;
use cryptoki::types::object::{Attribute, AttributeType};
use std::time::SystemTime;
use std::io;

use sequoia_openpgp as openpgp;
use openpgp::packet::{Key, signature};
use openpgp::packet::key::{Key4, PublicParts, SubordinateRole, UnspecifiedRole};
use openpgp::types::{HashAlgorithm, SignatureType};
use openpgp::serialize::Marshal;
use openpgp::Cert;
use openpgp::parse::Parse;

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "create-backsig")]
struct Opt {
    /// PIN to access the card
    #[structopt(short, long, default_value = "112233")]
    pin: String,

    #[structopt(long, default_value = "0")]
    index: usize,

    #[structopt(short, long)]
    id: u8,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();
    let stdin = io::stdin();
    let cert = Cert::from_reader(stdin)?;

    let (pkcs11, slot) = pkcs11_openpgp::init_pins(&opt.pin)?;

    // set flags
    let mut flags = Flags::new();
    let _ = flags.set_rw_session(true).set_serial_session(true);

    // open a session
    let session = pkcs11.open_session_no_callback(slot, flags)?;

    // log in the session
    session.login(UserType::User)?;

    let public_exponent: Vec<u8> = vec![0x01, 0x00, 0x01];
    let modulus_bits = 2048;

    // pub key template
    let pub_key_template = vec![
        Attribute::Token(true.into()),
        Attribute::Private(false.into()),
        Attribute::PublicExponent(public_exponent),
        Attribute::ModulusBits(modulus_bits.into()),
	Attribute::Id(vec![opt.id].into()),
    ];

    let mut objects = session.find_objects(&pub_key_template)?;

    eprintln!("{:?}", objects);

    for object in &objects {
	eprintln!("Object: {:?}", object);
	let attributes = session.get_attributes(*object, &[AttributeType::Label]);
	eprintln!("Attributes: {:?}", attributes);
	if let Ok(values) = attributes {
	if let Attribute::Label(str) = &values[0] {
	    eprintln!(" STR = {}", String::from_utf8_lossy(&str));
	}
	}
	eprintln!();
    }

    let object = objects.remove(opt.index);

    let attributes = session.get_attributes(object, &[AttributeType::Modulus, AttributeType::PublicExponent, AttributeType::Label])?;

    let mut key_modulus: Option<Vec<u8>> = None;
    let mut key_exponent: Option<Vec<u8>> = None;

    for attribute in attributes {
	match attribute {
	    Attribute::Modulus(modulus) => {
		eprintln!("GOT MODULUS = {:X?}", modulus);
		key_modulus = Some(modulus)
	    },
	    Attribute::PublicExponent(exponent) => {
		eprintln!("GOT EXPONENT = {:X?}", exponent);
		key_exponent = Some(exponent);
	    },
	    Attribute::Label(str) => eprintln!("USING = {}", String::from_utf8_lossy(&str)),
	    _ => {},
	}
    }

    if let (Some(modulus), Some(exponent)) = (key_modulus, key_exponent) {
        let key4: Key<PublicParts, SubordinateRole> =
            Key4::import_public_rsa(&exponent, &modulus, SystemTime::UNIX_EPOCH)?.into();


    // priv key template
    let priv_key_template = vec![Attribute::Token(true.into()),
				 Attribute::Private(true.into()), Attribute::Sign(true.into()), Attribute::Id(vec![opt.id].into())];

    let objects = session.find_objects(&priv_key_template)?;

    eprintln!("prv = {:?}, size = {}", objects, objects.len());

    for object in &objects {
	eprintln!("prv = Object: {:?}", object);
	let attributes = session.get_attributes(*object, &[AttributeType::Label]);
	eprintln!("prv = Attributes: {:?}", attributes);
	if let Ok(values) = attributes {
	    if let Attribute::Label(str) = &values[0] {
		eprintln!("prv = STR = {}", String::from_utf8_lossy(&str));
	    }
	}
	eprintln!();
    }

        let mut subkey_signer = PkcsKeyPair {
            public: &key4.role_as_unspecified(),
	    session,
	    private_object: &objects[0],
        };

        let backsig = signature::SignatureBuilder::new(SignatureType::PrimaryKeyBinding)
            .set_signature_creation_time(SystemTime::now())?
            .set_hash_algo(HashAlgorithm::SHA256)
            .sign_primary_key_binding(&mut subkey_signer, &cert.primary_key(), &key4)?;

        let mut stdout = io::stdout();
        backsig.serialize(&mut stdout)?;
    } else {
	eprintln!("did not get both");
    }
    
    Ok(())
}

struct PkcsKeyPair<'a> {
    public: &'a Key<PublicParts, UnspecifiedRole>,
    private_object: &'a ObjectHandle,
    session: Session<'a>,
}

impl openpgp::crypto::Signer for PkcsKeyPair<'_> {
    fn public(&self) -> &Key<PublicParts, UnspecifiedRole> {
        self.public
    }

    fn sign(
        &mut self,
        hash_algo: HashAlgorithm,
        digest: &[u8],
    ) -> openpgp::Result<openpgp::crypto::mpi::Signature> {
        assert_eq!(hash_algo, HashAlgorithm::SHA256);

	eprintln!("Key handle = {:?}", self.private_object);

	use picky_asn1_x509::{AlgorithmIdentifier, DigestInfo, SHAVariant};

	let digest: Vec<_> = digest.into();
	
	let info = picky_asn1_der::to_vec(&DigestInfo {
	    oid: AlgorithmIdentifier::new_sha(SHAVariant::SHA2_256),
	    digest: digest.into(),
	})?;
	
	let signature = self.session.sign(&Mechanism::RsaPkcs, *self.private_object, &info)?;

	//self.session.verify(&Mechanism::RsaPkcs, *self.public_object, digest, &signature)?;

        Ok(openpgp::crypto::mpi::Signature::RSA { s: signature.into() })
    }
}
