use cryptoki::types::locking::CInitializeArgs;
use cryptoki::types::session::UserType;
use cryptoki::types::slot_token::Slot;
use cryptoki::types::Flags;
use cryptoki::Pkcs11;

use std::env;

pub fn init_pins(pin: &str) -> Result<(Pkcs11, Slot), Box<dyn std::error::Error>> {
    let pkcs11 = Pkcs11::new(
        env::var("PKCS11_SOFTHSM2_MODULE")
            .unwrap_or_else(|_|
			    //"/usr/lib/softhsm/libsofthsm2.so"
			    "/usr/lib/libykcs11.so"
			    .to_string()),
    )?;

    // initialize the library
    pkcs11.initialize(CInitializeArgs::OsThreads)?;

    eprintln!("Get slots: {:?}", pkcs11.get_slots_with_token()?);

    // find a slot, get the first one
    let slot = pkcs11.get_slots_with_token().unwrap().remove(0);
    eprintln!("Slot: {:?}", slot);

    pkcs11.set_pin(slot, &pin)?;

    // set flags
    let mut flags = Flags::new();
    let _ = flags.set_rw_session(false).set_serial_session(true);

    {
        // open a session
        let session = pkcs11.open_session_no_callback(slot, flags)?;
        // log in the session
        session.login(UserType::User)?;
    }

    eprintln!("Slot OK.");

    Ok((pkcs11, slot))
}
