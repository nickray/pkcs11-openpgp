# PKCS#11 tools for OpenPGP

This crate allows using PIV tokens (e.g. Yubikeys) through PKCS#11 interface for OpenPGP operations.
It is like OpenPGP Card that GnuPG natively supports but using a different set of tools.

## Why?

PKCS#11 is widely deployed and more popular than OpenPGP Card.
Using this crate one can use their keys stored in a PIV applet for signing and decryption.

## Status

This crate is currently a *Proof of Concept* showing that it can be done.
Do not use this in production!

## Usage

Currently this crate assumes you have an OpenPGP primary key and will use PKCS#11 keys as OpenPGP subkeys.

Instructions below are for testing. All examples assume that compiled tools are in $PATH! (They will usually be under `target/debug`).

Create primary key:

    $ sq key generate --cannot-encrypt --cannot-sign --userid "<test-pkcs11@metacode.biz>" --export primary.sec.asc

Extract public parts from the key:

    $ sq key extract-cert < primary.sec.asc | sq dearmor > primary.pub.pgp

### Binding PKCS#11 smartcard key

Use `ykman-gui` tool (or any other) to generate keys on the card. `ykman-gui` requires creating certificates that as a side-effect also generates keys. This is OK for testing purposes.

*Note:* We could generate keys on the card programmatically.

See which objects are available:

```
$ list-objects
Get slots: [Slot { slot_id: 0 }]
Slot: Slot { slot_id: 0 }
Slot OK.
Object: ObjectHandle { handle: 87 }
 ID: [2]
 Label: Private key for Digital Signature

Object: ObjectHandle { handle: 88 }
 ID: [3]
 Label: Private key for Key Management

Object: ObjectHandle { handle: 110 }
 ID: [25]
 Label: Private key for PIV Attestation

Object: ObjectHandle { handle: 112 }
 ID: [2]
 Label: Public key for Digital Signature

Object: ObjectHandle { handle: 113 }
 ID: [3]
 Label: Public key for Key Management

Object: ObjectHandle { handle: 135 }
 ID: [25]
 Label: Public key for PIV Attestation
```

Remember IDs. `Digital Signature` keys are for "signing". `Key Management` for "decryption".

Then, export the subkey in OpenPGP format:

    $ export-subkey --pin 112233 --id 2 > subkey.pub.pgp

If your subkey is signing capable you also need to generate a back signature:

    $ create-backsig --pin 112233 --id 2 < primary.pub.pgp > backsig.pgp

Now, bind the subkey to the primary key:

    $ bind-subkey --backsig backsig.pgp --subkey subkey.pub.pgp < primary.sec.asc > subkey-binding.pgp

If there is no backsig (e.g. encryption capable subkey) just omit the `--backsig` parameter.

Now, concatenate all parts of the key:

    $ cat primary.pub.pgp subkey.pub.pgp subkey-binding.pgp > complete-key.pgp

`complete-key` can now be imported, uploaded to keyserver or shared as usual.

### Signing data

First, create a file to be signed:

    $ echo dummy test > file.txt

Then sign it via PKCS#11 interface:

    $ detach-sign --pin 112233 --id 2 < file.txt > file.txt.sig

And that's it! File is ready to be verified:

```
$ gpg --verify file.txt.sig
gpg: assuming signed data in 'file.txt'
gpg: Signature made Wed, 17 May 2021, 13:13:41 CET
gpg:                using RSA key EBA81FA1C3D96433EC91F31F83643509205B12FE
gpg: Good signature from "<test-pkcs11@metacode.biz>" [unknown]
```

### Decrypting data

First, add encryption subkey using key with a different ID. In our case it's `3` for `Key Management` key:

    $ export-subkey --pin 112233 --id 3 > subkey.pub.pgp
	
Not creating a backsig, since this is not signing-capable key and binding it directly:

    $ bind-subkey --subkey subkey.pub.pgp < primary.sec.asc > subkey-binding.pgp
	
And assembling all parts together:

    $ cat primary.pub.pgp subkey.pub.pgp subkey-binding.pgp > complete-key.pgp

Now, you can import `complete-key.pgp`.

As for the actual encryption. First, encrypt the data:

    $ echo dummy test | gpg -ear test-pkcs11@metacode.biz > encrypted.pgp

Now, pass the decrypted file to `decrypt`:

    $ decrypt --pin 112233 --id 3 < encrypted.pgp
	... debugging info omitted ...
	dummy test

## PKCS library

You will need a PKCS library. By default `/usr/lib/libykcs11.so` is used. This can be customized with `PKCS11_SOFTHSM2_MODULE` environment variable.

## Caveats

Currently only RSA 2048 is supported.
